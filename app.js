var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var coolRouter = require('./routes/cool');
const catalogRouter = require("./routes/catalog");
const heroRouter = require("./routes/hero");
const heroesRouter = require("./routes/heroes");
const petRouter = require("./routes/pet");
const petsRouter = require("./routes/pets");

var cors = require('cors')

var app = express()

app.use(cors())

// Set up mongoose connection
const mongoose = require("mongoose");
mongoose.set('strictQuery', false);
const mongoDB = "mongodb+srv://martimparaiba:Rack-Recede6-Compactly@cluster0.bjeqh6m.mongodb.net/local_library?retryWrites=true&w=majority";

main().catch(err => console.log(err));
async function main() {
  await mongoose.connect(mongoDB);
}


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// app.use(bodyParser.urlencoded({extended : true}));
app.use(express.json());

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/users/cool', coolRouter);
app.use('/catalog', catalogRouter);

app.use('/hero', heroRouter);
app.use('/heroes', heroesRouter);
app.use('/pet', petRouter);
app.use('/pets', petsRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;


// mongodb+srv://martimparaiba:Rack-Recede6-Compactly@cluster0.bjeqh6m.mongodb.net/local_library?retryWrites=true&w=majority