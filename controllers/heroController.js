const Hero = require("../models/hero");

const async = require("async");
const { isValidObjectId } = require("mongoose");

// Display list of all Heros.
exports.hero_list = function (req, res, next) {
  Hero.find().exec(function (err, list_heroes) {
    if (err) {
      return next(err);
    }
    //Successful, so render
    res.send(list_heroes);
  });
};

exports.get_by_id = function (req, res, next) {
    Hero.findById(req.params.id).exec(function (err,hero){
        res.send(hero);
    });
};

exports.new_hero = async function (req, res, next) {
    

    herodetails = {name: req.body.name}

    if (req.body.pet != false) herodetails.pet = req.body.pet;

    const hero = new Hero(herodetails);
    
    console.log(hero);

    await hero.save();
    console.log(`Added hero: ` + hero.name);

    res.send(hero);
};

exports.update_hero = function (req, res, next) {
    herodetails = {name: req.body.name, pet: req.body.pet};

    
    if (isValidObjectId(req.params.id)) {
        console.log("AQUI");
        Hero.updateOne({_id: req.params.id}, herodetails).exec( function (err, result) {

            if (err) return next(err);

            res.send(true);
        });
        return;
    }

    res.send("Invalid id");
};

exports.remove_hero = function (req, res, next) {
    if (isValidObjectId(req.params.id)) {
        var hero = Hero.remove({_id: req.params.id}).exec(function (err, hero ) {
            if (err) {
                res.send(err);
            }

            res.send(true);

        });

    } else {
        res.send("Invalid id");
    }
}