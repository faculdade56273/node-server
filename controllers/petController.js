const Pet = require("../models/pet");
const Hero = require("../models/hero");

const async = require("async");

exports.pet_list = function (req, res, next) {
  Pet.find().exec(function (err, list_pets) {
    if (err) {
      return next(err);
    }
    //Successful, so render
    res.send(list_pets);
  });
};

exports.get_by_id = function (req, res, next) {
  Pet.findById(req.params.id).exec(function (err, pet) {
    res.send(pet);
  });
};

exports.deletePet = function (req, res) {
  Hero.find({ pet: req.params.id }).exec(function (err, herolist) {
    if (err) {
      return next(err);
    }
    if (herolist.length === 0) {
      Pet.deleteOne({ _id: req.params.id }).exec();
    }
    res.send();
  });
};
