const async = require("async");
const mongoose = require("mongoose");

exports.populate = async function (req, res, next) {
  console.log("Debug: About to connect");
  console.log("Debug: Should be connected?");

  const Hero = require("../models/hero");
  const Pet = require("../models/pet");

  const pets = [];

  mongoose.set("strictQuery", false); // Prepare for Mongoose 7

  await Hero.deleteMany({});
  await Pet.deleteMany({});

  await createPets();
  await createHeroes();
  await createHeroesWithPets();

  console.log("Debug: Closing mongoose");

  async function petCreate(name) {
    const pet = await new Pet({ name: name });
    pet.save();
    pets.push(pet);
  }

  async function heroCreate(name) {
    await new Hero({ name: name }).save();
  }

  async function heroCreateWithPet(name, i) {
    await new Hero({ name: name, pet: pets[i]._id }).save();
  }

  async function createPets() {
    console.log("Adding Pets...");
    await Promise.all([
      petCreate("Patusca"),
      petCreate("Bobby"),
      petCreate("Snoop dog"),
      petCreate("Patrick Estrela"),
    ]);
  }

  async function createHeroes() {
    console.log("Adding Heroes...");
    await Promise.all([
      heroCreate("Super Homem"),
      heroCreate("Batman"),
      heroCreate("Homem aranha"),
      heroCreate("Catwoman"),
      heroCreate("Thor"),
    ]);
  }

  async function createHeroesWithPets() {
    console.log("Adding Heroes with Pets...");
    await Promise.all([
      heroCreateWithPet("Homem de Ferro", 1),
      heroCreateWithPet("Capitão América", 2),
    ]);
  }

  res.send();
};
