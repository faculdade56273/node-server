var express = require('express');
var router = express.Router();

const initController = require('../controllers/initController');

// GET home page.
router.get("/", function (req, res) {
  res.redirect("/catalog");
});

router.get("/init", initController.populate);

module.exports = router;

