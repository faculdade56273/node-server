const express = require("express");

const router = express.Router();

const heroController = require("../controllers/heroController.js");

router.post("/", heroController.new_hero);

router.get("/:id", heroController.get_by_id);

router.put("/:id", heroController.update_hero)

router.delete("/:id", heroController.remove_hero)

module.exports = router;