const express = require("express");
const router = express.Router();

const heroController = require("../controllers/heroController.js");

router.get("/", heroController.hero_list);

module.exports = router;