const express = require("express");
const router = express.Router();

const petController = require("../controllers/petController.js");


// GET pet with given id
router.get("/:id", petController.get_by_id);

router.delete("/:id", petController.deletePet);

module.exports = router;