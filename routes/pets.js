const express = require("express");
const router = express.Router();

const petController = require("../controllers/petController.js");

// GET list of pets
router.get("/", petController.pet_list);

module.exports = router;