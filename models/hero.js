const mongoose = require("mongoose");
// var Pet = require("Pet");

const Schema = mongoose.Schema;

const HeroSchema = new Schema({
  name: { type: String, required: true },
  pet: { type: Schema.Types.ObjectId, ref: "Pet", required: false },
});

// Virtual for hero's URL
HeroSchema.virtual("url").get(function () {
  // We don't use an arrow function as we'll need the this object
  return `/hero/${this._id}`;
});

// Export model
module.exports = mongoose.model("Hero", HeroSchema);
